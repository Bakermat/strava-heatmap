from flask import Flask
from flask import render_template
import csv
import json

app = Flask(__name__)

@app.route('/')
def my_runs():
    runs = []
    rides = []
    swims = []
    with open("example.csv", "r") as runs_file:
        reader = csv.DictReader(runs_file)

        for row in reader:
            if row["type"] == "Run":
                runs.append(row["polyline"])
            if row["type"] == "Ride":
                rides.append(row["polyline"])
            if row["type"] == "Swim":
                swims.append(row["polyline"])

    return render_template("leaflet.html", runs = json.dumps(runs), rides = json.dumps(rides), swims = json.dumps(swims))

if __name__ == "__main__":
    app.run(port = 5001)
