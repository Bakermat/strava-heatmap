import requests
import os
import sys
import csv

# Manually get the Strava access token and paste it here, or use an environment variable
#token = os.environ["TOKEN"]
token = "put_your_strava_access_token_here"
headers = {'Authorization': "Bearer {0}".format(token)}

with open("example.csv", "w") as activities_file:
    writer = csv.writer(activities_file, delimiter=",")
    writer.writerow(["id", "type", "polyline"])

    page = 1
    while True:
        r = requests.get("https://www.strava.com/api/v3/athlete/activities?page={0}".format(page), headers = headers)
        response = r.json()

        if len(response) == 0:
            break
        else:
            for activity in response:
                if (activity["type"] == "Run") or (activity["type"] == "Ride"):
                    r = requests.get("https://www.strava.com/api/v3/activities/{0}?include_all_efforts=true".format(activity["id"]), headers = headers)
                    polyline = r.json()["map"]["polyline"]
                    writer.writerow([activity["id"], activity["type"], polyline])
            page += 1
